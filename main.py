import mimetypes
import shutil
from fastapi import FastAPI, Response, File, UploadFile
from fastapi.middleware.cors import CORSMiddleware
import os
from typing import List
import uuid
import csscompressor
import zipfile
from fastapi.responses import FileResponse
from fastapi.staticfiles import StaticFiles

app = FastAPI(debug=False, docs_url=None, openapi_url=None)

origins = [
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:5173"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/assets", StaticFiles(directory="public/assets"), name="assets")

BASE_UPLOAD_DIRECTORY = "uploads/"
BASE_COMPRESSOR_DIRECTORY = "compressor/"


@app.get("/")
async def root():
    return FileResponse("public/index.html")


@app.get("/vite.svg")
async def get_vite_svg():
    return FileResponse("public/assets/vite.svg")


@app.post("/compressor")
async def compressor(file: List[UploadFile] = File(...)):
    unique_id = str(uuid.uuid4())
    upload_directory = os.path.join(BASE_UPLOAD_DIRECTORY, unique_id)
    compressor_directory = os.path.join(BASE_COMPRESSOR_DIRECTORY, unique_id)
    os.makedirs(upload_directory, exist_ok=True)
    os.makedirs(compressor_directory, exist_ok=True)

    file_paths = []
    file_path_name = []
    for f in file:
        if f.filename.endswith(".css"):
            file_path = os.path.join(upload_directory, f.filename)
            with open(os.path.join(upload_directory, f.filename), "wb") as buffer:
                shutil.copyfileobj(f.file, buffer)
            file_path_name.append(f.filename)
            file_paths.append(file_path)

    file_path_composser = []
    for file_path in file_paths:

        with open(file_path, 'r') as f:
            css_content = f.read()

        compressed_css = csscompressor.compress(css_content)

        with open(os.path.join(compressor_directory, f"{file_path_name[file_paths.index(file_path)][:-4]}.min.css"), 'w') as f:
            f.write(compressed_css)

        file_path_composser.append(os.path.join(compressor_directory, f"{
                                   file_path_name[file_paths.index(file_path)][:-4]}.min.css"))

    zip_name = f"{unique_id}.zip"
    zip_file_path = os.path.join(compressor_directory, zip_name)
    with zipfile.ZipFile(zip_file_path, 'w', zipfile.ZIP_DEFLATED) as zipf:
        # Рекурсивно добавляем файлы из указанной директории в ZIP-архив
        for root, _, files in os.walk(compressor_directory):
            for file_name in files:
                file_path = os.path.join(root, file_name)
                # Исключаем файлы с расширением .zip из архивации
                if not file_name.endswith('.zip'):
                    # Определяем относительный путь в архиве
                    arcname = os.path.relpath(
                        file_path, start=compressor_directory)
                    zipf.write(file_path, arcname=arcname)

    return {"zipfile": zip_name}


@app.get("/compressor/{file_path}")
async def download_file(file_path: str):
    # Строим полный путь к файлу
    full_path = os.path.join(BASE_COMPRESSOR_DIRECTORY,
                             file_path[:-4], file_path)

    filename = os.path.basename(full_path)
    # Открываем файл и считываем его содержимое
    with open(full_path, mode='rb') as file:
        file_content = file.read()

    # Определяем MIME-тип файла
    mime_type, _ = mimetypes.guess_type(full_path)

    # Если MIME-тип не удалось определить, используем общий тип
    if not mime_type:
        mime_type = 'application/octet-stream'

    # Возвращаем файл как ответ с правильными заголовками
    response = Response(content=file_content, media_type=mime_type)
    response.headers["Content-Disposition"] = f"attachment; filename={
        filename}"
    return response


if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app="main:app", host="0.0.0.0", port=8008, workers=4)
