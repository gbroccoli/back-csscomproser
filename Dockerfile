FROM python:3.12

WORKDIR /app

COPY ./ /app/

RUN pip install --no-cache-dir -r /app/requirements.txt

CMD ["python", "main.py"]
# RUN python main.py